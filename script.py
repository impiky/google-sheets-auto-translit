from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from transliterate import get_translit_function

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '1ujkj3CEwvVxGPJUvh93qVr8mEjcBX0voLW2LS5F3Zy0'
RANGE_NAME = 'A:N'


def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    cell_number = 1
    translit_ru = get_translit_function("ru")
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_secret.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    

    if not values:
        print('No data found.')
    else:
        # print('Name, Major:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            # print('%s, %s, %s, %s, %s' % (row[0], row[1], row[2], row[3], row[4]))
            values_to_write = [[translit_ru(row[1], reversed=True)], [translit_ru(row[2], reversed=True)], [translit_ru(row[3], reversed=True)], [translit_ru(row[4], reversed=True)]]
            # translited_region = [[translit_ru(row[2], reversed=True),]]
            # translited_city = [[translit_ru(row[3], reversed=True),]]
            # translited_street = [[translit_ru(row[4], reversed=True),]]
            # target_cells = "J{}:K{}:L{}:M{}:N{}".format(cell_number, cell_number, cell_number, cell_number, cell_number)
            target_cells = [["J{}".format(cell_number)], ["K{}".format(cell_number)], ["L{}".format(cell_number)], ["M{}".format(cell_number)], ["N{}".format(cell_number)]]
            body = {"values":values_to_write}
            sheet.values().update(spreadsheetId=SPREADSHEET_ID, valueInputOption="USER_ENTERED",
                                        range=target_cells, body=body).execute()
            cell_number += 1                            

if __name__ == '__main__':
    main()